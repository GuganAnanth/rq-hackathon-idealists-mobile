import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../core/theme/app_colors.dart';
import '../core/theme/app_styles.dart';

Widget blueButton ({required String buttonText, required Size size, required void Function() onTap}){
  return InkWell(
    onTap: onTap,
    child: Container(
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: <Color>[
            AppColors.pageBGColor,
            AppColors.buttonGradientColor
          ]
        ),
        borderRadius: BorderRadius.circular(8.0),
      ),
      height: 50.sp,
      width: size.width,
      alignment: Alignment.center,
      child: Text(buttonText, style: AppStyle.black15SemiboldTextStyle.copyWith(color: AppColors.white, fontSize: 20)),
    ),
  );
}


Widget blackButton ({required String buttonText, required Size size, required void Function() onTap}){
  return InkWell(
    onTap: onTap,
    child: Container(
      decoration: BoxDecoration(
        color: AppColors.black,
        borderRadius: BorderRadius.circular(8.0),
      ),
      height: 50.sp,
      width: size.width,
      alignment: Alignment.center,
      child: Text(buttonText, style: AppStyle.black15SemiboldTextStyle.copyWith(color: AppColors.white, fontSize: 20)),
    ),
  );
}

Widget redButton ({required String buttonText, required Size size, required void Function() onTap}){
  return InkWell(
    onTap: onTap,
    child: Container(
      decoration: BoxDecoration(
        color: AppColors.buttonRedColor,
        borderRadius: BorderRadius.circular(8.0),
      ),
      height: 50.sp,
      width: size.width,
      alignment: Alignment.center,
      child: Text(buttonText, style: AppStyle.black15SemiboldTextStyle.copyWith(color: AppColors.redTextColor, fontSize: 20)),
    ),
  );
}