
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

import '../core/theme/app_colors.dart';
import 'dart_helper.dart';

class ToastHelper {

  void getErrorFlushBar(String? text, BuildContext context) {
    Flushbar(
        flushbarPosition: FlushbarPosition.BOTTOM,
        margin: const EdgeInsets.only(bottom: 24, right: 15, left: 15),
        flushbarStyle: FlushbarStyle.FLOATING,
        messageColor: AppColors.white,
        borderRadius: BorderRadius.circular(10),
        borderColor: const Color(0xFFD65641),
        messageSize: 16,
        message: DartHelper.isNullOrEmpty(text??'') ? 'something went wrong':text,
        backgroundColor: AppColors.themeColor,
        isDismissible: false,
        mainButton: TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text('Ok',
                style: TextStyle(
                    fontFamily: 'Regualr',
                    fontSize: 16,
                    color: AppColors.white))))
        .show(context);
  }

// static Widget getToast(String text, double position, Function onTap,
  //     {bool autoDismiss = false}) {
  //   return GFFloatingWidget(
  //     verticalPosition: position,
  //     horizontalPosition: 10.0,
  //     child: GFToast(
  //       backgroundColor: AppColors.themeColor,
  //       text: text,
  //       textStyle: AppStyle.white14RegularTextStyle,
  //       button: GFButton(
  //         onPressed: onTap,
  //         text: 'OK',
  //         textStyle: AppStyle.white14RegularTextStyle,
  //         type: GFButtonType.transparent,
  //         color: GFColors.SUCCESS,
  //       ),
  //       type: GFToastType.rounded,
  //       autoDismiss: autoDismiss,
  //     ),
  //   );
  // }
  //
  // static Widget getDismissibleToast(String text, double position,
  //     {bool autoDismiss = true}) {
  //   return GFFloatingWidget(
  //     verticalPosition: position,
  //     horizontalPosition: 10.0,
  //     child: GFToast(
  //       backgroundColor: AppColors.textFieldBorderColor,
  //       text: text,
  //       textStyle: AppStyle.black15SemiboldTextStyle,
  //       type: GFToastType.rounded,
  //       autoDismiss: autoDismiss,
  //       autoDismissDuration: Duration(milliseconds: 7000),
  //     ),
  //   );
  // }
}
