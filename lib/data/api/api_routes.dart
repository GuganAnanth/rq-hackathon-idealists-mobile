import 'api_client.dart';

class ApiRoutes {
  static const ApiConfig debugConfig = ApiConfig(
    scheme: 'https',
    host: 'api.uat.velichamgrow.com',
//    port: 443,
    scope: scope,
  );

  static const ApiConfig prodConfig = ApiConfig(
    scheme: 'https',
    host: 'api.velichamgrow.com',
    port: 443,
    scope: scope,
  );

  //Scope
  static const String debugScope = '';
  static const String scope = '/api/v1';
}
