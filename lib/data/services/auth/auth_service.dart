import 'dart:async';
import '../../../models/api_success.dart';
import '../../api/api_client.dart';
import '../api_service.dart';

class AuthService extends ApiService {
  AuthService({required ApiClient client}) : super(client: client);

//************************************ log-in *********************************//
  Future<Map<String, dynamic>> loginWithPassword(
      {Map<String, dynamic>? objToApi}) async {
    final ApiResponse<ApiSuccess> res = await client!.callJsonApi<ApiSuccess>(
        method: Method.POST,
        headers: <String, String>{
          'Content-Type': 'application/json',
        },
        path: '/user_management/employee/login',
        body: objToApi);
    if (res.isSuccess) {
      return {
        'customer' : res.data!.user,
        'token' : res.data!.token,
      };
    } else {
      throw res.error!;
    }
  }
}
