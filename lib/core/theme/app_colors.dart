import 'package:flutter/material.dart';

class AppColors {
  static const Color themeColor = Color(0xFF024242);
  static const Color bgColor = Color(0XFFF2F2E5);
  static const Color textFieldBorderColor = Color(0XFFD3D3D3);
  static const Color textFieldFillColor = Color(0XFFF6F6F6);
  static const Color textFieldTextColor = Color(0XFF4F4F4F);
  static const Color textFieldTitleColor = Color(0XFF9E9E9E);
  static const Color checkBoxGreenColor = Color(0XFF00B53E);
  static const Color yellowColor = Color(0XFFF3CB5E);
  static const Color greenColor = Color(0XFF26AD10);
  static const Color invitePendingColor = Color(0XFFF3A65E);
  static const Color blueColor = Color(0XFF058AFF);
  static const Color bajajColor = Color(0XFF0074BF);
  static const Color bajajPaymentModeColor = Color(0XFFEFF9FF);

  static const Color white = Color(0xFFFFFFFF);
  static const Color semiBoldTextColor = Color(0xFF010101);
  static const Color greyBackgroundColor = Color(0xFFF3F5F7);
  static const Color greyLabelColor = Color(0xFFAFB4BB);
  static const Color iconGreenColor = Color(0xFF77E6B5);
  static const Color lightGreenBGColor = Color(0xFFF2F8F8);
  static const Color greyTextColor = Color(0xFF525252);
  static const Color arcColor = Color(0xFF79E5B5);
  static const Color arcBGColor = Color(0xFF165151);
  static const Color greyColor2 = Color(0xFFA3BABA);
  static const Color greyColor3 = Color(0xFF849691);
  static const Color searchIconColor = Color(0xFFD8E5E1);
  static const Color filterColor = Color(0xFFA3B4B0);
  static const Color blueColor2 = Color(0xFF2276EF);
  static const Color milkGreenColor2 = Color(0xFF22888D);
  static const Color coinBGColor = Color(0xFFFFF59A);
  static const Color pageBGColor = Color(0xFF044CCD);
  static const Color greyRegularTextColor = Color(0xFFA2A2A2);
  static const Color buttonGradientColor = Color(0xFF022667);
  static const Color borderWhite = Color(0xFFD4D4D4);
  static const Color textColor = Color(0xFF9EA5AD);
  static const Color forgotPasswordColor = Color(0xFF005BFF);

  static const Color black = Color(0xFF000000);
  static const Color violetColor = Color(0xFFDCE8FF);
  static const Color milkGreenColor = Color(0xFFDDFFEB);
  static const Color sandalColor = Color(0xFFFFF6D8);
  static const Color babyPinkColor = Color(0xFFFFEEF5);
  static const Color coinsBGColor = Color(0xFFFFF5D3);
  static const Color dividerColor = Color(0xFFDDDDDD);
  static const Color textGreyColor = Color(0xFF454C52);
  static const Color buttonRedColor = Color(0xFFFFF3F3);
  static const Color redTextColor = Color(0xFFEC5F5F);
}
