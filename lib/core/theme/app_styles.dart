import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'app_colors.dart';

class AppStyle {

  //**************************** white - color ********************************//
  static TextStyle milkGreen12TextStyle = TextStyle(fontFamily: 'Regular', fontSize: 14.sp, color: AppColors.milkGreenColor2);
  static TextStyle white14RegularTextStyle = TextStyle(fontFamily: 'Regular', fontSize: 14.sp, color: Colors.white);
  static TextStyle blue14RegularTextStyleItalic = TextStyle(fontFamily: 'Regular', fontSize: 14.sp, fontStyle: FontStyle.italic, color: AppColors.blueColor2);
  static TextStyle white14SemiboldTextStyle = TextStyle(fontFamily: 'SemiBold', fontSize: 14.sp, color: Colors.white);
  static TextStyle white15RegularTextStyle = TextStyle(fontFamily: 'Regular', fontSize: 15.sp, color: Colors.white);
  static TextStyle white17RegularTextStyle = TextStyle(fontFamily: 'Regular', fontSize: 17.sp, color: Colors.white);
  static TextStyle white15SemiBoldTextStyle = TextStyle(fontFamily: 'SemiBold', fontSize: 15.sp, color: Colors.white);
  static TextStyle white20MediumTextStyle = TextStyle(fontFamily: 'Medium', fontSize: 20.sp, color: Colors.white);

  //**************************** black - color ********************************//
  static TextStyle black14RegularTextStyle = TextStyle(fontFamily: 'Regular', fontSize: 14.sp, color: AppColors.textFieldTextColor);
  static TextStyle black15SemiboldTextStyle = TextStyle(fontFamily: 'Semibold', fontWeight: FontWeight.w500, fontSize: 15.sp, color: AppColors.semiBoldTextColor);
  static TextStyle black28BoldTextStyle = TextStyle(fontFamily: 'Bold', fontSize: 28.sp, color: AppColors.textFieldTextColor);
  static TextStyle black24BoldTextStyle = TextStyle(fontFamily: 'Bold', fontSize: 24.sp, color: AppColors.semiBoldTextColor);
  static TextStyle black16MediumTextStyle = TextStyle(fontFamily: 'Medium', fontSize: 16.sp, color: AppColors.textFieldTextColor);

  //***************************** grey - color ********************************//
  static TextStyle grey14RegularTextStyle = TextStyle(fontFamily: 'Regular', fontSize: 14.sp, color: AppColors.textFieldTitleColor);
  static TextStyle blue14UnderlinedTextStyleItalic = TextStyle(fontFamily: 'Regular', fontSize: 14.sp, decoration: TextDecoration.underline, color: AppColors.blueColor2);
  static TextStyle grey16RegularTextStyle = TextStyle(fontFamily: 'Regular', fontSize: 16.sp, color: AppColors.greyLabelColor);

  //**************************** purple - color *******************************//
  static TextStyle purple28BoldTextStyle = TextStyle(fontFamily: 'Bold', fontSize: 28.sp, color: AppColors.themeColor);
  static TextStyle purple32SemiboldTextStyle = TextStyle(fontFamily: 'SemiBold', fontSize: 32.sp, color: AppColors.themeColor);
  static TextStyle purple16SemiBoldTextStyle = TextStyle(fontFamily: 'SemiBold', fontSize: 16.sp, color: AppColors.themeColor);
  static TextStyle purple20SemiBoldTextStyle = TextStyle(fontFamily: 'SemiBold', fontSize: 20.sp, color: AppColors.themeColor);
  static TextStyle purple15MediumTextStyle = TextStyle(fontFamily: 'Medium', fontSize: 15.sp, color: AppColors.themeColor);
}
