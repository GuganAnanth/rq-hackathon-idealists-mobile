class AppAssets {
  AppAssets._();

  static const String rqLogo = 'assets/images/rq_logo.png';
  static const String homePageCoin = 'assets/images/home_page_coin.png';
  static const String revuCoin = 'assets/images/revu_coin.png';
  static const String bgGrid = 'assets/images/bg_grid.png';
  static const String checkMail = 'assets/images/check_mail.png';
  static const String personPlaceholder = 'assets/images/person_placeholder.jpg';
  static const String finoBuddyLogo = 'assets/images/finoBuddy.png';
  static const String vgroLogo = 'assets/images/vgro_logo.png';
  static const String riseLogo = 'assets/images/rise.png';
  static const String bddLogo = 'assets/images/bdd.png';

  static const String coloredStar = 'assets/images/colored_star.png';
  static const String unColoredStar = 'assets/images/uncolored_star.png';
}
