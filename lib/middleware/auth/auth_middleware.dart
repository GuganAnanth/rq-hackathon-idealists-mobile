import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import '../../actions/auth/auth_action.dart';
import '../../data/app_repository.dart';
import '../../data/services/auth/auth_service.dart';
import '../../global_widgets/toast_helper.dart';
import '../../models/api_error.dart';
import '../../models/app_state.dart';
import '../../models/app_user.dart';
import '../../views/home/home_page.dart';

class AuthMiddleware {
  AuthMiddleware({required this.repository})
      : authService = repository.getService<AuthService>() as AuthService;

  final AppRepository repository;
  final AuthService authService;

  List<Middleware<AppState>> createAuthMiddleware() {
    return <Middleware<AppState>>[
      TypedMiddleware<AppState, CheckForUserInPrefs>(checkForUserInPrefs),
      TypedMiddleware<AppState, LoginWithPassword>(loginWithPassword),
      TypedMiddleware<AppState, LogOutUser>(logOutUser)
    ];
  }

  void checkForUserInPrefs(Store<AppState> store, CheckForUserInPrefs action,
      NextDispatcher next) async {
    next(action);
    try {
      final AppUser? user = await repository.getUserFromPrefs();

      if (user != null) {
        store.dispatch(SetInitializer(false));
        store.dispatch(SaveUser(userDetails: user));
      } else {
        store.dispatch(SetInitializer(false));
        store.dispatch(SaveUser(userDetails: null));
      }
    } catch (e) {
      return;
    }
  }

  void loginWithPassword(Store<AppState> store, LoginWithPassword action,
      NextDispatcher next) async {
    try {
      String registrationToken = '';
      store.dispatch(new SetLoader(true));
      final Map<String, dynamic> objToApi = <String, dynamic>{
        'employee': <String, String>{
          'email': action.email!,
          'password': action.password!,
          'grant_type': 'password'
        }
      };
      final Map<String, dynamic> response =
          await authService.loginWithPassword(objToApi: objToApi);
      print(response['error']);
      final AppUser user = response['customer'];
      repository.setUserAccessToken(accessToken: response['token']);
      repository.setUserPrefs(appUser: user!);
      print("==========first==============${user.toString()}");
      print(
          "=========second===================${response['employee'].toString()}");
      if (user != null) {
        store.dispatch(SaveUser(userDetails: user));
        store.state.navigator.currentState!
            .push(MaterialPageRoute(builder: (context) => HomePage()));
        print(
            "=============in state =========${store.state.currentUser.toString()}");
      }else{
        print('No user');
      }
      store.dispatch(SetLoader(false));
    } on ApiError catch (e) {
      ToastHelper().getErrorFlushBar(e.errorMessage, store.state.navigator.currentContext!);
      debugPrint('============ login error block ========== ${e.toString()}');
      store.dispatch(SetLoader(false));
      //  globalErrorAlert(
      //      store.state.navigator.currentContext, e?.errorMessage, null);
      return;
    } catch (e) {
      ToastHelper().getErrorFlushBar(e.toString(), store.state.navigator.currentContext!);
      store.dispatch(SetLoader(false));
      debugPrint('============ login catch block ========== ${e}');
    }
    next(action);
  }

  void logOutUser(
      Store<AppState> store, LogOutUser action, NextDispatcher next) async {
    repository.setUserPrefs(appUser: null);
    store.dispatch(SaveUser(userDetails: null));
    next(action);
  }
}
