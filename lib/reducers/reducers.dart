import 'package:redux/redux.dart';

import '../models/app_state.dart';
import 'auth/auth_reducer.dart';

Reducer<AppState> reducer = combineReducers(<Reducer<AppState>>[
  authReducer,
]);
