import 'package:flutter/material.dart';
import 'package:rq_revu/views/Init/welcome_page.dart';

import '../../connector/auth_connector.dart';
import '../home/home_page.dart';

class InitPage extends StatelessWidget {
  const InitPage({super.key});

  @override
  Widget build(BuildContext context) {
    return AuthConnector(builder: (BuildContext context, AuthViewModel authViewModel) {
        return authViewModel.currentUser == null ? const WelcomePage() : const HomePage();
      },
    );
  }
}
