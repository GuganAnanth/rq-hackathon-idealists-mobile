import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rq_revu/views/Init/login_page.dart';

import '../../core/theme/app_assets.dart';
import '../../core/theme/app_colors.dart';
import '../../core/theme/app_styles.dart';
import '../../global_widgets/blue_button.dart';
import '../../global_widgets/form_validation_helper.dart';
import '../../global_widgets/widget_helper.dart';
import '../home/home_page.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _idController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _anotherPasswordController = TextEditingController();

  final GlobalKey<FormState> formKey = GlobalKey();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _nameController.dispose();
    _idController.dispose();
    _anotherPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.white,
        floatingActionButton: InkWell(
            child: Container(
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: AppColors.white,
                    boxShadow: [
                      BoxShadow(
                        color: AppColors.textColor.withOpacity(0.2),
                        spreadRadius: 5,
                        blurRadius: 2,
                      )
                    ]
                ),
                padding: EdgeInsets.all(5.sp),
                margin: EdgeInsets.only(top: 40.sp),
                child:  Icon(Icons.arrow_back, color: AppColors.black, size: 24.sp)
            ),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
        body: Column(
          children: [
            Expanded(child: Container(
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(AppAssets.bgGrid),
                      fit: BoxFit.cover
                  )
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Row(),
                  getSpace(100.sp, 0),
                  Image.asset(AppAssets.rqLogo, height: 40.sp, width: 174.sp),
                  Expanded(child: emptyBox()),
                ],
              ),
            )),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(25.sp), topRight: Radius.circular(25.sp)),
                  boxShadow: [BoxShadow(color: AppColors.pageBGColor.withOpacity(0.1), blurRadius: 20.0, spreadRadius: 10.0)],
                  color: AppColors.white
              ),
              margin: EdgeInsets.symmetric(horizontal: 16.0.sp),
              padding: EdgeInsets.all(16.0.sp),
              child: Form(
                key: formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Signup', style: AppStyle.black15SemiboldTextStyle.copyWith(fontSize: 32.sp)),
                    getSpace(14.0.sp, 0),
                    Row(
                      children: [
                        Expanded(child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Name', style: AppStyle.black14RegularTextStyle),
                            getSpace(8.0.sp, 0),
                            Stack(
                              children: [
                                Container(
                                  height: 45.sp,
                                  decoration: BoxDecoration(
                                    color: AppColors.white,
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(color: AppColors.borderWhite, width: 1),
                                  ),
                                ),
                                TextFormField(
                                  controller: _nameController,
                                  keyboardType: TextInputType.text,
                                  autovalidateMode: AutovalidateMode.onUserInteraction,
                                  style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                                  inputFormatters: [
                                    FilteringTextInputFormatter.allow(RegExp(r'[a-z A-Z]'))
                                  ],
                                  validator: (String? val) {
                                    if((val != null) || (val?.isNotEmpty ?? false)){
                                      return FormValidationHelper().emptyValidation(val!, 'Enter a valid name.');
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.person, size: 24.sp, color: AppColors.textColor),
                                    hintText: 'Name',
                                    hintStyle: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 8.sp),
                                    border: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    fillColor: Colors.transparent,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )),
                        getSpace(0, 14.sp),
                        Expanded(child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Employee ID', style: AppStyle.black14RegularTextStyle),
                            getSpace(8.0.sp, 0),
                            Stack(
                              children: [
                                Container(
                                  height: 45.sp,
                                  decoration: BoxDecoration(
                                    color: AppColors.white,
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(color: AppColors.borderWhite, width: 1),
                                  ),
                                ),
                                TextFormField(
                                  controller: _idController,
                                  keyboardType: TextInputType.text,
                                  autovalidateMode: AutovalidateMode.onUserInteraction,
                                  style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                                  inputFormatters: [
                                    FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9]'))
                                  ],
                                  validator: (String? val) {
                                    if((val != null) || (val?.isNotEmpty ?? false)){
                                      return FormValidationHelper().emptyValidation(val!, 'Enter a valid ID.');
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    hintText: 'ID',
                                    hintStyle: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 8.sp),
                                    border: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    fillColor: Colors.transparent,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )),
                      ],
                    ),
                    getSpace(12.sp, 0),
                    Text('Email', style: AppStyle.black14RegularTextStyle),
                    getSpace(8.0.sp, 0),
                    Stack(
                      children: [
                        Container(
                          height: 45.sp,
                          decoration: BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: AppColors.borderWhite, width: 1),
                          ),
                        ),
                        TextFormField(
                          controller: _emailController,
                          keyboardType: TextInputType.text,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                          validator: (String? val) {
                            if((val != null) || (val?.isNotEmpty ?? false)){
                              return FormValidationHelper().emailValidation(val!);
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.email_outlined, size: 24.sp, color: AppColors.textColor),
                            hintText: 'hello@rootquotient.com',
                            hintStyle: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 8.sp),
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            fillColor: Colors.transparent,
                          ),
                        ),
                      ],
                    ),
                    getSpace(24.0.sp, 0),
                    Text('Set Password', style: AppStyle.black14RegularTextStyle),
                    getSpace(8.0.sp, 0),
                    Stack(
                      children: [
                        Container(
                          height: 45.sp,
                          decoration: BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: AppColors.borderWhite, width: 1),
                          ),
                        ),
                        TextFormField(
                          controller: _passwordController,
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                          validator: (String? val) {
                            if((val != null) || (val?.isNotEmpty ?? false)){
                              return FormValidationHelper().emptyValidation(val!, 'Password cannot be empty');
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.lock_outline, size: 22.sp, color: AppColors.textColor),
                            hintText: 'password',
                            hintStyle: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 8.sp),
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            fillColor: Colors.transparent,
                          ),
                        ),
                      ],
                    ),
                    getSpace(24.0.sp, 0),
                    Text('Confirm Password', style: AppStyle.black14RegularTextStyle),
                    getSpace(8.0.sp, 0),
                    Stack(
                      children: [
                        Container(
                          height: 45.sp,
                          decoration: BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: AppColors.borderWhite, width: 1),
                          ),
                        ),
                        TextFormField(
                          controller: _anotherPasswordController,
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                          validator: (String? val) {
                            if((val != null) || (val?.isNotEmpty ?? false)){
                              return FormValidationHelper().emptyValidation(val!, 'Password cannot be empty');
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.lock_outline, size: 22.sp, color: AppColors.textColor),
                            hintText: 'password',
                            hintStyle: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 8.sp),
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            fillColor: Colors.transparent,
                          ),
                        ),
                      ],
                    ),
                    getSpace(24.0.sp, 0),
                    blueButton(buttonText: 'Register', onTap: (){
                      if(formKey.currentState?.validate() ?? false){
                        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const HomePage()));
                      }
                    }, size: MediaQuery.of(context).size),
                    getSpace(12.0.sp, 0),
                    Center(child: RichText(
                      text: TextSpan(
                          text: 'already a user? ',
                          style: AppStyle.black14RegularTextStyle.copyWith(color: AppColors.greyRegularTextColor),
                          children: <TextSpan>[
                            TextSpan(
                                text: 'Login',
                                style: AppStyle.black15SemiboldTextStyle,
                                recognizer: TapGestureRecognizer()..onTap = (){
                                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
                                }
                            )
                          ]
                      ),
                    ))
                  ],
                ),
              ),
            )
          ],
        )
    );
  }
}
