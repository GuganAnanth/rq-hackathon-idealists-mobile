import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rq_revu/global_widgets/widget_helper.dart';
import 'package:rq_revu/views/Init/sign_up_page.dart';

import '../../core/theme/app_assets.dart';
import '../../core/theme/app_colors.dart';
import '../../core/theme/app_styles.dart';
import '../../global_widgets/blue_button.dart';
import 'login_page.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({super.key});

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> with SingleTickerProviderStateMixin{

  late Animation animation;

  @override
  void initState() {
    super.initState();
    AnimationController controller = AnimationController(vsync: this, duration: const Duration(seconds: 1));
    animation = Tween<double>(begin: 300, end: 0).animate(CurvedAnimation(parent: controller, curve: Curves.fastEaseInToSlowEaseOut));
    controller.forward();
    controller.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: Column(
        children: [
          const Row(),
          getSpace(100.sp, 0),
          Image.asset(AppAssets.rqLogo, height: 40.sp, width: 174.sp),
          Expanded(child: emptyBox()),
          Transform(
            transform: Matrix4.translationValues(0, animation.value, 0),
            child: Stack(
              children: [
                Positioned(
                  right: 0,
                  left: 0,
                  bottom: 80,
                  child: Container(
                    decoration: const BoxDecoration(
                        gradient: RadialGradient(
                            radius: 0.6,
                            colors: <Color>[
                              AppColors.coinBGColor,
                              AppColors.white
                            ]
                        )
                    ),
                    padding: EdgeInsets.only(left: 30.sp, right: 40.sp, bottom: 15.sp),
                    child: Image.asset(AppAssets.revuCoin, height: 240.sp, width: 160.sp),
                  ),
                ),
                Image.asset(AppAssets.bgGrid, height: 340.sp, width: MediaQuery.of(context).size.width)
              ],
            )
          ),
          Expanded(child: emptyBox()),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(25.sp), topRight: Radius.circular(25.sp)),
                boxShadow: [BoxShadow(color: AppColors.pageBGColor.withOpacity(0.1), blurRadius: 20.0, spreadRadius: 10.0)],
                color: AppColors.white
            ),
            margin: EdgeInsets.symmetric(horizontal: 16.0.sp),
            padding: EdgeInsets.all(24.0.sp),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Get Started!', style: AppStyle.black15SemiboldTextStyle.copyWith(fontSize: 32)),
                getSpace(8.0.sp, 0),
                Text('Download, Explore, Review & provide Feedbacks of RQ UAT apps to earn points', style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 12, color: AppColors.greyRegularTextColor)),
                getSpace(24.0.sp, 0),
                blueButton(buttonText: 'Login', onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const LoginPage()));
                }, size: MediaQuery.of(context).size),
                getSpace(12.0.sp, 0),
                Center(child: RichText(
                  text: TextSpan(
                      text: 'New here? ',
                      style: AppStyle.black14RegularTextStyle.copyWith(color: AppColors.greyRegularTextColor),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Sign up',
                            style: AppStyle.black15SemiboldTextStyle,
                            recognizer: TapGestureRecognizer()..onTap = (){
                              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const SignUpPage()));
                            }
                        )
                      ]
                  ),
                )),
              ],
            ),
          )
        ],
      )
    );
  }
}
