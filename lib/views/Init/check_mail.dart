import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../core/theme/app_assets.dart';
import '../../core/theme/app_colors.dart';
import '../../core/theme/app_styles.dart';
import '../../global_widgets/blue_button.dart';
import '../../global_widgets/widget_helper.dart';

class CheckMail extends StatelessWidget {
  const CheckMail({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.white,
        floatingActionButton: InkWell(
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: AppColors.white,
                boxShadow: [
                  BoxShadow(
                    color: AppColors.textColor.withOpacity(0.2),
                    spreadRadius: 5,
                    blurRadius: 2,
                  )
                ]
              ),
                padding: EdgeInsets.all(5.sp),
                margin: EdgeInsets.only(top: 40.sp),
                child:  Icon(Icons.arrow_back, color: AppColors.black, size: 24.sp)
            ),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
        body: Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppAssets.bgGrid),
                  fit: BoxFit.cover
              )
          ),
          padding: EdgeInsets.symmetric(horizontal: 16.sp),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Row(),
              getSpace(100.sp, 0),
              Image.asset(AppAssets.rqLogo, height: 40.sp, width: 174.sp),
              Expanded(child: emptyBox()),
              Image.asset(AppAssets.checkMail, height: 296.sp, width: 296.sp),
              ColoredBox(
                color: AppColors.white,
                child: Text('Check Mail', style: AppStyle.black16MediumTextStyle.copyWith(fontSize: 32.sp)),
              ),
              Container(
                color: AppColors.white,
                padding: EdgeInsets.symmetric(horizontal: 5.sp, vertical: 5.sp),
                margin: EdgeInsets.symmetric(horizontal: 20.sp),
                child: Text('Steps to reset your password has been send to your email.', textAlign: TextAlign.center, style: AppStyle.black14RegularTextStyle.copyWith(color: AppColors.greyRegularTextColor, fontSize: 12)),
              ),
              Expanded(flex: 2, child: emptyBox()),
              blackButton(buttonText: 'Go back', size: MediaQuery.of(context).size, onTap: (){
                Navigator.pop(context);
                Navigator.pop(context);
              }),
              getSpace(20.sp, 0),
            ],
          ),
        )
    );
  }
}
