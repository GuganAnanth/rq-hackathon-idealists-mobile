import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rq_revu/views/Init/welcome_page.dart';

import '../../core/theme/app_assets.dart';
import '../../core/theme/app_colors.dart';
import '../../core/theme/app_styles.dart';
import '../../global_widgets/blue_button.dart';
import '../../global_widgets/widget_helper.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.black,
      body: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16.0), bottomRight: Radius.circular(16.0)),
                    color: AppColors.white
                ),
                clipBehavior: Clip.hardEdge,
                padding: EdgeInsets.symmetric(horizontal: 24.sp),
                child: Padding(
                  padding: EdgeInsets.only(top: 50.sp, bottom: 16.sp),
                  child: Row(
                    children: [
                      InkWell(
                          child: const Icon(Icons.arrow_back, color: AppColors.black, size: 24),
                          onTap: (){
                            Navigator.pop(context);
                          }
                      ),
                      getSpace(0, 15.sp),
                      Text('Profile', style: AppStyle.black24BoldTextStyle.copyWith(fontSize: 16.sp))
                    ],
                  ),
                ),
              ),
              getSpace(160.sp, 0),
              Expanded(child: Container(
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(topRight: Radius.circular(16.0), topLeft: Radius.circular(16.0)),
                    color: AppColors.white
                ),
                padding: const EdgeInsets.all(24.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    getSpace(90.sp, 0),
                    Text('Name', style: AppStyle.black16MediumTextStyle.copyWith(color: AppColors.textGreyColor, fontSize: 14.sp)),
                    Text('User', style: AppStyle.black24BoldTextStyle.copyWith(fontSize: 20.sp)),
                    getSpace(15.sp, 0),
                    Divider(color: AppColors.dividerColor, height: 15.sp),
                    getSpace(15.sp, 0),
                    Text('Employee ID', style: AppStyle.black16MediumTextStyle.copyWith(color: AppColors.textGreyColor, fontSize: 14.sp)),
                    Text('RQ0000', style: AppStyle.black24BoldTextStyle.copyWith(fontSize: 20.sp)),
                    getSpace(15.sp, 0),
                    Divider(color: AppColors.dividerColor, height: 15.sp),
                    getSpace(15.sp, 0),
                    Text('Email', style: AppStyle.black16MediumTextStyle.copyWith(color: AppColors.textGreyColor, fontSize: 14.sp)),
                    Text('abc@gmail.com', style: AppStyle.black24BoldTextStyle.copyWith(fontSize: 20.sp)),
                    Expanded(child: emptyBox()),
                    redButton(buttonText: 'Logout', size: MediaQuery.of(context).size, onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => WelcomePage()));
                    }),
                  ],
                ),
              ))
            ],
          ),
          Positioned(
            top: 150.sp,
            left: MediaQuery.of(context).size.width / 4,
            child: Container(
            decoration: const BoxDecoration(
                color: AppColors.white,
                shape: BoxShape.circle
            ),
            height: 180,
            clipBehavior: Clip.hardEdge,
            child: Image.asset(AppAssets.personPlaceholder, fit: BoxFit.contain),
          )
          )
        ],
      ),
    );
  }
}
