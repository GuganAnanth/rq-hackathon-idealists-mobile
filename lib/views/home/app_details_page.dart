import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rq_revu/global_widgets/blue_button.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../core/theme/app_assets.dart';
import '../../core/theme/app_colors.dart';
import '../../core/theme/app_styles.dart';
import '../../global_widgets/widget_helper.dart';
import 'feedback_page.dart';

class AppDetailsPage extends StatelessWidget {
  const AppDetailsPage({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.black,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16.0), bottomRight: Radius.circular(16.0)),
                color: AppColors.white
            ),
            clipBehavior: Clip.hardEdge,
            padding: EdgeInsets.symmetric(horizontal: 24.sp),
            child: Padding(
              padding: EdgeInsets.only(top: 50.sp, bottom: 16.sp),
              child: Row(
                children: [
                  InkWell(
                      child: const Icon(Icons.arrow_back, color: AppColors.black, size: 24),
                      onTap: (){
                        Navigator.pop(context);
                      }
                  ),
                  getSpace(0, 15.sp),
                  Text('Product', style: AppStyle.black24BoldTextStyle.copyWith(fontSize: 16.sp))
                ],
              ),
            ),
          ),
          getSpace(15.sp, 0),
          Expanded(child: Container(
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(topRight: Radius.circular(16.0), topLeft: Radius.circular(16.0)),
                color: AppColors.white
            ),
            padding: const EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(title, style: AppStyle.black24BoldTextStyle.copyWith(fontSize: 28.sp)),
                    getSpace(0, 5.sp),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        color: AppColors.coinsBGColor
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 8.sp, vertical: 3.sp),
                      child: Row(
                        children: [
                          Image.asset(AppAssets.revuCoin, fit: BoxFit.contain, width: 14.sp, height: 14.sp),
                          Text('30', style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 10.sp))
                        ],
                      ),
                    ),
                    Expanded(child: emptyBox()),
                    InkWell(
                      onTap: (){
                        if(title == 'Finobuddy'){
                          final String finoBuddyUrl = 'https://drive.google.com/file/d/1Xfp6J9V7IvX8SG8my7BgWs-L2Zykjsnu/view?usp=sharing';
                          launchUrl(Uri.parse(finoBuddyUrl), mode: LaunchMode.externalApplication);
                        }
                      },
                      child: Text('Download', style: AppStyle.black15SemiboldTextStyle.copyWith(color: AppColors.black, decoration: TextDecoration.underline, fontSize: 12)),
                    )
                  ],
                ),
                getSpace(12.sp, 0),
                Expanded(child: SingleChildScrollView(child: Text('About $title, a few description has to be provided. Points can be redeemed after testing a particular feature. Make sure to properly go through the feature. If you are struck at any part, make sure to note it down, and provide it in the Feedback. If you have any doubts about the flow, make sure to mention it in the Feedback that the flow is confusing. \n \nNote that when you are providing your Feedback, please include as many details as possible. Points are allocated based on your useful of the Feedback. If your Feedback brings greater value to the product, you get more points. Meanwhile, not providing a valid Feedback or a vague Feedback will give you less points. Make sure to include as many details in the Feedback. Mention what you tested clearly, and what your observations are. Points are distributed based on the usefulness of the Feedback. If your Feedback is considered to be a spam, you will not be get any points.', style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 12, color: AppColors.greyRegularTextColor)))),
                getSpace(12.sp, 0),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Releasing on', style: AppStyle.black15SemiboldTextStyle.copyWith(color: AppColors.pageBGColor, fontSize: 16.sp)),
                        Text('Month 2024', style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 12, color: AppColors.greyRegularTextColor))
                      ],
                    )),
                    getSpace(0, 10.sp),
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Market Area', style: AppStyle.black15SemiboldTextStyle.copyWith(color: AppColors.pageBGColor, fontSize: 16.sp)),
                        Text('India / USA / Canada', style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 12, color: AppColors.greyRegularTextColor))
                      ],
                    )),
                    getSpace(0, 10.sp),
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Industry', style: AppStyle.black15SemiboldTextStyle.copyWith(color: AppColors.pageBGColor, fontSize: 16.sp)),
                        Text('Fin-Tech', style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 12, color: AppColors.greyRegularTextColor))
                      ],
                    )),
                  ],
                ),
                getSpace(24.sp, 0),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Managed by ', style: AppStyle.black15SemiboldTextStyle.copyWith(color: AppColors.pageBGColor, fontSize: 16.sp)),
                        Text('Project Manager', style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 12, color: AppColors.greyRegularTextColor))
                      ],
                    )),
                    getSpace(0, 10.sp),
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Project type', style: AppStyle.black15SemiboldTextStyle.copyWith(color: AppColors.pageBGColor, fontSize: 16.sp)),
                        Text('T&M', style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 12, color: AppColors.greyRegularTextColor))
                      ],
                    )),
                    getSpace(0, 10.sp),
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Team Size', style: AppStyle.black15SemiboldTextStyle.copyWith(color: AppColors.pageBGColor, fontSize: 16.sp)),
                        Text('4-5', style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 12, color: AppColors.greyRegularTextColor))
                      ],
                    )),
                  ],
                ),
                getSpace(12.sp, 0),
                Divider(color: AppColors.dividerColor, height: 15.sp),
                Text('Features', style: AppStyle.black15SemiboldTextStyle.copyWith(fontSize: 20)),
                getSpace(12.sp, 0),
                Row(
                  children: [
                    Expanded(child: Row(
                      children: [
                        Icon(Icons.circle, color: AppColors.black, size: 5.sp),
                        getSpace(0, 7.sp),
                        Text('Signup', style: AppStyle.black15SemiboldTextStyle)
                      ],
                    )),
                    Expanded(child: Row(
                      children: [
                        Icon(Icons.circle, color: AppColors.black, size: 5.sp),
                        getSpace(0, 7.sp),
                        Text('Feature A', style: AppStyle.black15SemiboldTextStyle)
                      ],
                    ))
                  ],
                ),
                getSpace(12.sp, 0),
                Row(
                  children: [
                    Expanded(child: Row(
                      children: [
                        Icon(Icons.circle, color: AppColors.black, size: 5.sp),
                        getSpace(0, 7.sp),
                        Text('Feature B', style: AppStyle.black15SemiboldTextStyle)
                      ],
                    )),
                    Expanded(child: Row(
                      children: [
                        Icon(Icons.circle, color: AppColors.black, size: 5.sp),
                        getSpace(0, 7.sp),
                        Text('Feature C', style: AppStyle.black15SemiboldTextStyle)
                      ],
                    ))
                  ],
                ),
                getSpace(12.sp, 0),
                blueButton(buttonText: 'Review', size: MediaQuery.of(context).size, onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => FeedBackPage()));
                }),
              ],
            ),
          ))
        ],
      ),
    );
  }
}
