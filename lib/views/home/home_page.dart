import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rq_revu/views/home/profile_page.dart';

import '../../core/theme/app_assets.dart';
import '../../core/theme/app_colors.dart';
import '../../core/theme/app_styles.dart';
import '../../global_widgets/widget_helper.dart';
import 'app_details_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final List<Color> colorsList = [AppColors.sandalColor, AppColors.milkGreenColor, AppColors.violetColor, AppColors.babyPinkColor];
  final List<String> imagesList = [AppAssets.finoBuddyLogo, AppAssets.vgroLogo, AppAssets.riseLogo, AppAssets.bddLogo];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.black,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16.0), bottomRight: Radius.circular(16.0)),
              color: AppColors.white
            ),
            clipBehavior: Clip.hardEdge,
            padding: EdgeInsets.symmetric(horizontal: 24.sp),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                getSpace(75.sp, 0),
                Row(
                  children: [
                    RichText(text: TextSpan(
                      text: 'Hi ',
                      style: AppStyle.black16MediumTextStyle.copyWith(fontSize: 32),
                      children: [
                        TextSpan(
                          text: 'User!',
                          style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 28)
                        )
                      ]
                    )),
                    Expanded(child: emptyBox()),
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ProfilePage()));
                      },
                      child: Container(
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle
                        ),
                        height: 46.sp,
                        width: 46.sp,
                        clipBehavior: Clip.hardEdge,
                        child: Image.asset(AppAssets.personPlaceholder)
                      ),
                    )
                  ],
                ),
                Text('Welcome back', style: AppStyle.black14RegularTextStyle.copyWith(color: AppColors.greyRegularTextColor)),
                getSpace(24.sp, 0),
                Row(
                  children: [
                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(text: TextSpan(
                          text: '0',
                          style: AppStyle.black14RegularTextStyle.copyWith(color: AppColors.pageBGColor, fontSize: 48),
                          children: [
                            TextSpan(
                              text: ' Points',
                              style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16, color: AppColors.pageBGColor)
                            )
                          ]
                        )),
                        getSpace(5.sp, 0),
                        RichText(text: TextSpan(
                          text: 'Points can be redeemed ',
                          style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 12),
                          children: [
                            TextSpan(
                              text: 'here',
                              style: AppStyle.black15SemiboldTextStyle.copyWith(fontSize: 12, decoration: TextDecoration.underline),
                              recognizer: TapGestureRecognizer()..onTap = (){

                              }
                            )
                          ]
                        ))
                      ],
                    )),
                    Container(
                      decoration: const BoxDecoration(
                          gradient: RadialGradient(
                              radius: 0.6,
                              colors: <Color>[
                                AppColors.coinBGColor,
                                AppColors.white
                              ]
                          )
                      ),
                      transform: Matrix4.translationValues(40, 0, 0),
                      padding: EdgeInsets.only(left: 10.sp, right: 10.sp),
                      child: Image.asset(AppAssets.revuCoin, height: 180.sp, width: 180.sp),
                    )
                  ],
                ),
              ],
            ),
          ),
          getSpace(40.sp, 0),
          Expanded(child: Container(
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(topRight: Radius.circular(16.0), topLeft: Radius.circular(16.0)),
                color: AppColors.white
            ),
            padding: const EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Row(),
                Text('Our Products', style: AppStyle.black15SemiboldTextStyle.copyWith(fontSize: 20)),
                getSpace(12.sp, 0),
                Expanded(child: GridView.builder(gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, mainAxisSpacing: 16.sp, childAspectRatio: 1.15, crossAxisSpacing: 16.sp), itemCount: imagesList.length, itemBuilder: (BuildContext context, int index){
                  return AppCard(color: colorsList[index], image: imagesList[index], onTap: onTapList(context, index));
                }))
              ],
            ),
          ))
        ],
      ),
    );
  }
}

Function() onTapList(BuildContext context, int index){
  switch (index){
    case 0:
      return (){
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const AppDetailsPage(title: 'Finobuddy')));
      };
    case 1:
      return (){
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const AppDetailsPage(title: 'VGro')));
      };
    case 2:
      return (){
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const AppDetailsPage(title: 'Rise')));
      };
    case 3:
      return (){
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const AppDetailsPage(title: 'BDD')));
      };
    default:
      return (){
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const AppDetailsPage(title: 'App Title')));
      };
  }
}

class AppCard extends StatelessWidget {
  const AppCard({super.key, required this.color, required this.image, required this.onTap});

  final Color color;
  final String image;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.sp),
            color: color
        ),
        alignment: Alignment.center,
        height: 105.sp,
        child: Image.asset(image, fit: BoxFit.contain, height: 52.sp),
      )
    );
  }
}
