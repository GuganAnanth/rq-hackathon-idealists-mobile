import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../core/theme/app_assets.dart';
import '../../core/theme/app_colors.dart';
import '../../core/theme/app_styles.dart';
import '../../global_widgets/blue_button.dart';
import '../../global_widgets/form_validation_helper.dart';
import '../../global_widgets/widget_helper.dart';

class FeedBackPage extends StatefulWidget {
  const FeedBackPage({super.key});

  @override
  State<FeedBackPage> createState() => _FeedBackPageState();
}

class _FeedBackPageState extends State<FeedBackPage> {

  final TextEditingController _featureNameController = TextEditingController();
  final TextEditingController _otherController = TextEditingController();
  final TextEditingController _reviewController = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey();

  int tappedIndex = 0;
  String? selectedImprovement;

  @override
  void dispose() {
    _featureNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.black,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16.0), bottomRight: Radius.circular(16.0)),
                color: AppColors.white
            ),
            clipBehavior: Clip.hardEdge,
            padding: EdgeInsets.symmetric(horizontal: 24.sp),
            child: Padding(
              padding: EdgeInsets.only(top: 50.sp, bottom: 16.sp),
              child: Row(
                children: [
                  InkWell(
                      child: const Icon(Icons.arrow_back, color: AppColors.black, size: 24),
                      onTap: (){
                        Navigator.pop(context);
                      }
                  ),
                  getSpace(0, 15.sp),
                  Text('Review', style: AppStyle.black24BoldTextStyle.copyWith(fontSize: 16.sp))
                ],
              ),
            ),
          ),
          getSpace(15.sp, 0),
          Expanded(child: Container(
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(topRight: Radius.circular(16.0), topLeft: Radius.circular(16.0)),
                color: AppColors.white
            ),
            padding: const EdgeInsets.all(24.0),
            child: Form(
              key: formKey,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Rate your experience', style: AppStyle.black24BoldTextStyle.copyWith(fontSize: 28.sp)),
                    getSpace(12.sp, 0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: List.generate(5, (index) => InkWell(
                        onTap: (){
                          tappedIndex = index+1;
                          setState(() {});
                        },
                        child: Image.asset(index+1 > tappedIndex ? AppAssets.unColoredStar : AppAssets.coloredStar, fit: BoxFit.contain, height: 45.sp, width: 45.sp),
                      )),
                    ),
                    getSpace(24.sp, 0),
                    Text('Name of the Feature', style: AppStyle.black16MediumTextStyle),
                    getSpace(8.sp, 0),
                    Stack(
                      children: [
                        Container(
                          height: 45.sp,
                          decoration: BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: AppColors.borderWhite, width: 1),
                          ),
                        ),
                        TextFormField(
                          controller: _featureNameController,
                          keyboardType: TextInputType.text,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp(r'[a-z A-Z]'))
                          ],
                          validator: (String? val) {
                            if((val != null) || (val?.isNotEmpty ?? false)){
                              return FormValidationHelper().emptyValidation(val!, 'Enter a valid name.');
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: 'Feature Name',
                            hintStyle: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 8.sp),
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            fillColor: Colors.transparent,
                          ),
                        ),
                      ],
                    ),
                    getSpace(12.sp, 0),
                    Text('Tell us what can be improved', style: AppStyle.black16MediumTextStyle),
                    getSpace(12.sp, 0),
                    Row(
                      children: [
                        InkWell(
                            onTap: (){
                              selectedImprovement = 'Functionality';
                              setState(() {});
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: selectedImprovement == 'Functionality' ? AppColors.pageBGColor : AppColors.textFieldFillColor
                              ),
                              padding: EdgeInsets.symmetric(horizontal: 24.sp, vertical: 10.sp),
                              child: Text('Functionality', style: AppStyle.black15SemiboldTextStyle.copyWith(fontSize: 12, color: selectedImprovement == 'Functionality' ? AppColors.white : AppColors.black)),
                            )
                        ),
                        getSpace(0, 15.sp),
                        InkWell(
                            onTap: (){
                              selectedImprovement = 'Experience';
                              setState(() {});
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: selectedImprovement == 'Experience' ? AppColors.pageBGColor : AppColors.textFieldFillColor
                              ),
                              padding: EdgeInsets.symmetric(horizontal: 24.sp, vertical: 10.sp),
                              child: Text('Experience', style: AppStyle.black15SemiboldTextStyle.copyWith(fontSize: 12, color: selectedImprovement == 'Experience' ? AppColors.white : AppColors.black)),
                            )
                        )
                      ],
                    ),
                    getSpace(12.sp, 0),
                    Row(
                      children: [
                        InkWell(
                            onTap: (){
                              selectedImprovement = 'Smoothness';
                              setState(() {});
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: selectedImprovement == 'Smoothness' ? AppColors.pageBGColor : AppColors.textFieldFillColor
                              ),
                              padding: EdgeInsets.symmetric(horizontal: 24.sp, vertical: 10.sp),
                              child: Text('Smoothness', style: AppStyle.black15SemiboldTextStyle.copyWith(fontSize: 12, color: selectedImprovement == 'Smoothness' ? AppColors.white : AppColors.black)),
                            )
                        ),
                        getSpace(0, 15.sp),
                        InkWell(
                            onTap: (){
                              selectedImprovement = 'Interaction';
                              setState(() {});
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: selectedImprovement == 'Interaction' ? AppColors.pageBGColor : AppColors.textFieldFillColor
                              ),
                              padding: EdgeInsets.symmetric(horizontal: 24.sp, vertical: 10.sp),
                              child: Text('Interaction', style: AppStyle.black15SemiboldTextStyle.copyWith(fontSize: 12, color: selectedImprovement == 'Interaction' ? AppColors.white : AppColors.black)),
                            )
                        )
                      ],
                    ),
                    getSpace(12.sp, 0),
                    InkWell(
                        onTap: (){
                          selectedImprovement = 'Others';
                          setState(() {});
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              color: selectedImprovement == 'Others' ? AppColors.pageBGColor : AppColors.textFieldFillColor
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 24.sp, vertical: 10.sp),
                          child: Text('Others', style: AppStyle.black15SemiboldTextStyle.copyWith(fontSize: 12, color: selectedImprovement == 'Others' ? AppColors.white : AppColors.black)),
                        )
                    ),
                    getSpace(12.sp, 0),
                    selectedImprovement == 'Others' ? Text(
                        'Specific, what others?', style: AppStyle.black16MediumTextStyle.copyWith(fontSize: 14.0)
                    ) : emptyBox(),
                    selectedImprovement == 'Others' ? getSpace(12.sp, 0) : emptyBox(),
                    selectedImprovement == 'Others' ? Stack(
                      children: [
                        Container(
                          height: 45.sp,
                          decoration: BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: AppColors.borderWhite, width: 1),
                          ),
                        ),
                        TextFormField(
                          controller: _otherController,
                          keyboardType: TextInputType.text,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp(r'[a-z A-Z]'))
                          ],
                          validator: (String? val) {
                            if((val != null) || (val?.isNotEmpty ?? false)){
                              return FormValidationHelper().emptyValidation(val!, 'Please enter a valid feature.');
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: 'Add Text',
                            hintStyle: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 8.sp),
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            fillColor: Colors.transparent,
                          ),
                        ),
                      ],
                    ) : emptyBox(),
                    selectedImprovement == 'Others' ? getSpace(12.sp, 0) : emptyBox(),
                    Text('Write your review', style: AppStyle.black16MediumTextStyle),
                    getSpace(8.sp, 0),
                    Stack(
                      children: [
                        Container(
                          height: 125.sp,
                          decoration: BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: AppColors.borderWhite, width: 1),
                          ),
                        ),
                        TextFormField(
                          controller: _reviewController,
                          keyboardType: TextInputType.multiline,
                          textInputAction: TextInputAction.newline,
                          maxLines: 4,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          style: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                          validator: (String? val) {
                            if((val != null) || (val?.isNotEmpty ?? false)){
                              return FormValidationHelper().emptyValidation(val!, 'Please write some review.');
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: 'Write Review',
                            hintStyle: AppStyle.black14RegularTextStyle.copyWith(fontSize: 16.sp, color: AppColors.textColor),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 8.sp),
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            fillColor: Colors.transparent,
                          ),
                        ),
                      ],
                    ),
                    getSpace(24.sp, 0),
                    blueButton(buttonText: 'Submit', size: MediaQuery.of(context).size, onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => FeedBackPage()));
                    }),
                  ],
                ),
              ),
            ),
          ))
        ],
      ),
    );
  }
}