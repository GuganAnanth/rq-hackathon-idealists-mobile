import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'actions/auth/auth_action.dart';
import 'data/api/api_routes.dart';
import 'data/app_repository.dart';
import 'data/preference_client.dart';
import 'middleware/middleware.dart';
import 'models/app_state.dart';
import 'reducers/reducers.dart';
import 'views/Init/init_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final AppRepository repository = AppRepository(preferencesClient: PreferencesClient(prefs: prefs), config: ApiRoutes.debugConfig);
  await SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
  ]);
  await ScreenUtil.ensureScreenSize();
  runApp(MyApp(repository: repository));
}

class MyApp extends StatefulWidget {
  MyApp({Key? key, required AppRepository repository})
      : store = Store<AppState>(reducer, middleware: middleware(repository), initialState: AppState.initState()), super(key: key);

  final Store<AppState> store;

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  late Store<AppState> store;

  @override
  void initState() {
    super.initState();
    store = widget.store;
    _init();
  }

  void _init() {
    Future<void>.delayed(const Duration(seconds: 2), () {
      store.dispatch(CheckForUserInPrefs());
    });
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: ScreenUtilInit(
        designSize: MediaQuery.of(context).size,
        builder: (context, widget){
          return MaterialApp(
            navigatorKey: store.state.navigator,
            title: 'RQ reVU',
            home: const InitPage(),
            debugShowCheckedModeBanner: false,
          );
        }
      ),
    );
  }
}
