import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'access_token.dart';
import 'app_user.dart';
import 'file_attachment.dart';
import 'pagination.dart';

part 'api_success.g.dart';

abstract class ApiSuccess implements Built<ApiSuccess, ApiSuccessBuilder> {
  factory ApiSuccess([ApiSuccessBuilder updates(ApiSuccessBuilder builder)]) =
      _$ApiSuccess;

  ApiSuccess._();

  int? get status;

  String? get message;

  AccessToken? get token;

  @BuiltValueField(wireName: 'employee')
  AppUser? get user;

//***************************** pagination ***********************************//
  Pagination? get meta;

//****************************** file-upload **********************************//

  @BuiltValueField(wireName: 'uploaded_file')
  FileAttachment? get uploadedFile;

  static Serializer<ApiSuccess> get serializer => _$apiSuccessSerializer;

}
