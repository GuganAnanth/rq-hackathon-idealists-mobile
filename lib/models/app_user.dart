
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'app_user.g.dart';

abstract class AppUser implements Built<AppUser, AppUserBuilder>{
  AppUser._();
  factory AppUser([AppUserBuilder updates(AppUserBuilder builder)]) = _$AppUser;

  static Serializer<AppUser> get serializer => _$appUserSerializer;
}